# Sollicitatie_IC3D_Media
Deze repository bevat drie mappen met code. De eerste map *ChoHan* bevat code die ervoor zorgen dat een simpele server het spelletje [Chō-han](https://en.wikipedia.org/wiki/Ch%C5%8D-han).
De tweede map: *Notificatiesysteem UWP* bevat code geschikt voor het Unified Windows Platform. Dit kan verschillende notificaties naar een UWP apparaat sturen.
De derde en laatste map: *Uwp mobile game* bevat code die binnen uwp een spel aanmaakt waarbij door middel van bing maps en de geo locatie mensen een punt kunnen veroveren. Hier is vooral game logica geschreven.