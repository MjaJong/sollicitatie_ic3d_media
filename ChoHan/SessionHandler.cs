﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using SharedUtilities;
using Timer = System.Timers.Timer;

namespace ChoHan
{
    /// <summary>
    /// The class for the session handler. The server uses these handlers to make sure everything runs smoothly.
    /// </summary>
    public class SessionHandler
    {
        public bool _isInterupted { get; set; }
        public string SessionName { get; set; }
        public readonly int MaxPlayers;
        public readonly List<Player> Players;

        private readonly Timer _awnserTimer;
        private readonly Timer _startTimer;

        private bool _gameGateKeeper;
        private bool _startGame;
        private bool _gameStart;
        private bool _gameGoesOn = true;

        private readonly Log _sessionLog;

        /// <summary>
        /// Constructor for the session handler. The session contains all the data and keeps track of the timers. It also starts a event log for the session.
        /// </summary>
        /// <param name="name">The name for the session that is being made.</param>
        /// <param name="maxPlayers">The maximum amount of players for this session.</param>
        public SessionHandler(string name, int maxPlayers)
        {
            SessionName = name;
            _sessionLog = new Log(SessionName + "_" + DateTime.Today);
            MaxPlayers = maxPlayers;
            Players = new List<Player>();
            _awnserTimer = new Timer(15000);
            _startTimer = new Timer(5000);

            _awnserTimer.Elapsed += (sender, args) =>
            {
                _gameGateKeeper = true;
                _awnserTimer.Stop();
            };

            _startTimer.Elapsed += (sender, args) =>
            {
                _startGame = true;
                _startTimer.Stop();
            };

            _sessionLog.AddLogEntry("Succesfully started a log.");
        }

        /// <summary>
        /// The method to add a player to the session whose method has been called. This method checks to see if the player can join the current session and gives the player trying 
        /// to join appropriate feedback. It also terminates players whose connection has died. 
        /// </summary>
        /// <param name="player">The player that wants to join the session.</param>
        public void AddPlayer(Player player)
        {
            if (Players.Count <= MaxPlayers && !_gameStart)
            {

                try
                {
                    Players.Add(player);
                    UpdatePlayerPanel(player, "Welcome to Cho Han");
                    Console.WriteLine($"Player {player.Naam} has joined the game: {SessionName}");
                    Server.SendSessions();
                    _sessionLog.AddLogEntry($"Added a player: {player.Naam}.");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MurderDeadConnection(player);
                }
            }
            else
            {
                try
                {
                    UpdatePlayerPanel(player, "To many players or the game has already started");
                    Console.WriteLine("To many players or the game has already started.");
                    _sessionLog.AddLogEntry($"Failed to add player: {player.Naam}.");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MurderDeadConnection(player);
                }
            }
        }

        /// <summary>
        /// Method to start a game. It takes no parameters and keeps running, taking guesses from the players, then rolling odd or even and checking the results. After five rounds
        /// or when the game can't go on any longer, the game will end. This gives the players their result, finalises the log for a game and ends the game
        /// Any connections to players that have fallen silent are automaticly disposed of so the program does not crash.
        /// </summary>
        private void StartGame()
        {
            int roundCount = 0;
            ChoHan game = new ChoHan();

            _sessionLog.AddLogEntry("Started a new game of ChoHan.");

            while (roundCount < 5 && _gameGoesOn)
            {
                _gameStart = true;
                int answercount = 0;
                Console.WriteLine("Waiting for players to confirm");
                //Waits for every client to choose an answer
                Console.WriteLine(roundCount);
                _awnserTimer.Start();
                while (answercount < Players.Count)
                {
                    if (_gameGateKeeper)
                    {
                        _gameGateKeeper = false;
                        break;
                    }
                    answercount = 0;
                    for (int i = 0; i < Players.Count; i++)
                    {
                        try
                        {
                            _sessionLog.AddLogEntry($"Send a confirmation message to {Players.ElementAt(i).Naam}.");
                            SharedUtil.SendMessage(Players.ElementAt(i).Client, new
                            {
                                id = "give/confirmation"
                            });
                            dynamic message = SharedUtil.ReadMessage(Players.ElementAt(i).Client);
                            bool answer = (bool) message.data.confirmation;
                            if (answer)
                            {
                                answercount++;
                            }
                            Console.WriteLine(answercount);
                            _sessionLog.AddLogEntry(Players.ElementAt(i).Naam, "Confirmed activity with the server.");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.StackTrace);
                            MurderDeadConnection(Players.ElementAt(i));
                        }
                    }
                }
                _awnserTimer.Stop();
                _gameGateKeeper = false;

                Console.WriteLine("Gimmy dat answer");
                //send every client a message that they can send their answer
                game.ThrowDice();

                for (int i = 0; i < Players.Count; i++)
                {
                    try
                    {
                        _sessionLog.AddLogEntry($"Asked {Players.ElementAt(i).Naam} for a awnser.");
                        SharedUtil.SendMessage(Players.ElementAt(i).Client, new
                        {
                            id = "give/answer"
                        });

                        dynamic answer = SharedUtil.ReadMessage(Players.ElementAt(i).Client);
                        _sessionLog.AddLogEntry(Players.ElementAt(i).Naam, "Gave the server an awnser.");
                        if (!(bool) answer.data.check)
                        {
                            UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.Idle());
                            UpdatePlayers(Players.ElementAt(i), false);
                            continue;
                        }

                        if ((bool) answer.data.answer)
                        {
                            if (game.CheckResult(true))
                            {
                                Players.ElementAt(i).Score++;
                                UpdatePlayers(Players.ElementAt(i), true);
                                UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.GoodAnswer());
                            }
                            else
                            {
                                UpdatePlayers(Players.ElementAt(i), false);
                                UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.WrongAnswer());
                            }
                            _sessionLog.AddLogEntry($"{Players.ElementAt(i).Naam}'s awnser has been proccesed.");
                        }
                        else
                        {
                            if (game.CheckResult(false))
                            {
                                Players.ElementAt(i).Score++;
                                UpdatePlayers(Players.ElementAt(i), true);
                                UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.GoodAnswer());
                            }
                            else
                            {
                                UpdatePlayers(Players.ElementAt(i), false);
                                UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.WrongAnswer());
                            }
                            _sessionLog.AddLogEntry($"{Players.ElementAt(i).Naam}'s awnser has been proccesed.");
                        }
                    }
                    catch
                    (Exception e)
                    {
                        Console.WriteLine(e.StackTrace);
                        MurderDeadConnection(Players.ElementAt(i));
                    }
                    Players.Sort((x, y) => y.Score - x.Score);
                }
                    //Sorts list on score and send it to the clients
                UpdatePlayerList();
                roundCount++;
            }
            Result();
            _sessionLog.PrintLog();
            GameEnded();
        }

        /// <summary>
        /// The result method calculates the results of a single round. It is a supporting method for Startgame() and will only be called by Startgame(). 
        /// Any connections to players that have fallen silent are automaticly disposed of so the program does not crash.
        /// </summary>
        private void Result()
        {
            bool playerOneWin = true;
            _sessionLog.AddLogEntry("Ranked players.");

            //starts looking for the ties and loses
            for (int i = 0; i < Players.Count; i++)
            {
                try
                {
                    _sessionLog.AddLogEntry($"Calculating {Players.ElementAt(i).Naam}'s score.");
                    if (Players.ElementAt(i).Equals(Players.ElementAt(0))) continue;

                    if (Players.ElementAt(i).Score - Players.ElementAt(0).Score == 0)
                    {
                        UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.Tied());
                        playerOneWin = false;
                    }

                    else if (Players.ElementAt(i).Score - Players.ElementAt(0).Score < 0)
                    {
                        UpdatePlayerPanel(Players.ElementAt(i), WittyAnswer.Lose());
                    }

                    else
                    {
                        Console.WriteLine("ffs are you here?!");
                        UpdatePlayerPanel(Players.ElementAt(i), "something went wrong here");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MurderDeadConnection(Players.ElementAt(i));
                }
            }
            _sessionLog.AddLogEntry("Calculated the score of all players.");
            Console.WriteLine("Winner determined");

            //checks if the highest score doesn't tie with another one
            Console.WriteLine("Is there a winner?");
            try
            {
                UpdatePlayerPanel(Players.ElementAt(0), playerOneWin ? WittyAnswer.Win() : WittyAnswer.Tied());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                UpdatePlayerPanel(Players.ElementAt(1), playerOneWin ? WittyAnswer.Win() : WittyAnswer.Tied());
            }

            _sessionLog.AddLogEntry("Crowned one of the suckers as a winner.");
        }

        /// <summary>
        /// The method to update the GUI on the player side of the game. Any connections to players that have fallen silent are automaticly disposed of so the program does not crash.
        /// </summary>
        /// <param name="player">The player whose GUI should be updated.</param>
        /// <param name="text">The text to write to the GUI.</param>
        public void UpdatePlayerPanel(Player player, string text)
        {
            try
            {
                SharedUtil.SendMessage(player.Client, new
                {
                    id = "update/panel",
                    data = new
                    {
                        text = text
                    }
                });
                SharedUtil.ReadMessage(player.Client);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);

                MurderDeadConnection(player);
            }
        }

        /// <summary>
        /// This method does the same as UpdatePlayerPanel(Player player, string text) only for every player at once. 
        /// Any connections to players that have fallen silent are automaticly disposed of so the program does not crash.
        /// </summary>
        /// <param name="text">The text to write to the panels.</param>
        public void UpdateAllPanels(string text)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                try
                {
                    SharedUtil.SendMessage(Players.ElementAt(i).Client, new
                    {
                        id = "update/panel",
                        data = new
                        {
                            text = text
                        }
                    });
                    SharedUtil.ReadMessage(Players.ElementAt(i).Client);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MurderDeadConnection(Players.ElementAt(1));
                }
            }
        }

        /// <summary>
        /// The method to read the awnser given by a player. Any connections to players that have fallen silent are automaticly disposed of so the program does not crash.
        /// </summary>
        /// <param name="player">The player who needs to be updated.</param>
        /// <param name="answer">The awnser to the player. This indicates wheter the guess was correct or not.</param>
        public void UpdatePlayers(Player player, bool answer)
        {
            try
            {
                SharedUtil.SendMessage(player.Client, new
                {
                    id = "recieve/answer",
                    data = new
                    {
                        score = player.Score,
                        answer = answer
                    }
                });
                SharedUtil.ReadMessage(player.Client);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                MurderDeadConnection(player);
            }
        }

        /// <summary>
        /// The thread for the session.
        /// </summary>
        public void SessionHandleThread()
        {
            while (true)
            {
                _gameGoesOn = true;
                _gameStart = false;
                if (Players.Count < 2)  continue;
                _startTimer.Start();
                if (!_startGame) continue;
                _startGame = false;
                StartGame();
            }
        }


        /// <summary>
        /// Override of the ToString() method.
        /// </summary>
        /// <returns>The string created by the ToString() method.</returns>
        public override string ToString()
        {
            return $"{SessionName}: {Players.Count}/{MaxPlayers}";
        }

        /// <summary>
        /// The method to update the list of player currently in this session.
        /// </summary>
        public void UpdatePlayerList()
        {
            foreach (var c in Players)
            {
                SharedUtil.SendMessage(c.Client, new
                {
                    id = "send/players",
                    data = new
                    {
                        players = Players.Select(s => s.ToString()).ToArray()
                    }
                });
            }
        }

        /// <summary>
        /// The support method to savely end a game. It is only called bij the Startgame() method. It disposes of all players in a save way so that the program does not chrash.
        /// </summary>
        private void GameEnded()
        {
            RemovePlayersFromSession();
            MurderPlayers();
            if (_gameGoesOn)
            {
                Server.SendSessions();
            }
        }

        /// <summary>
        /// The support method to remove players from a session. It is only called bij the GameEnded() method.
        /// </summary>
        private void RemovePlayersFromSession()
        {
            for (int i = 0; i < Players.Count; i++)
            {
                try
                {
                    _sessionLog.AddLogEntry($"Murdered {Players.ElementAt(i).Naam}.");
                    SharedUtil.SendMessage(Players.ElementAt(i).Client, new
                    {
                        id = "session/leave"
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MurderDeadConnection(Players.ElementAt(i));
                }
            }
        }

        /// <summary>
        /// The support method to "kill" all players. This method is only called by the GameEnded() method.
        /// </summary>
        private void MurderPlayers()
        {
            foreach (var c in Players)
            {
                c.IsSession = false;
                c.Score = 0;
            }
            Players.Clear();
        }

        /// <summary>
        /// //The function to "kill" the connection with a player that has fallen silent. When the player count falls low enough it will stop a session in progress.
        /// </summary>
        /// <param name="p">The player whose connection that has gone silent.</param>
        public void MurderDeadConnection(Player p)
        {
            _sessionLog.AddLogEntry($"Kicked {p.Naam} from the game.");
            p.IsRipped = true;
            Players.RemoveAll(i => i.Equals(p));


            if (Players.Count < 2)
            {
                _gameGoesOn = false;
                _isInterupted = true;
                UpdateAllPanels("Game has stopped and starts again");

                _sessionLog.AddLogEntry("Not enough players, ending a game.");
                
                UpdatePlayerPanel(Players.ElementAt(0), "Idiot has left the game");
                _sessionLog.PrintLog();
                GameEnded();
                Server.DeleteSession(this);
            }
        }
    }
}


