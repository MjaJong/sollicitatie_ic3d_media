﻿using SharedUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ChoHan
{
    /// <summary>
    /// The class for the server. This runs on a host and handles all the sessions started and all clients connecting to the server.
    /// </summary>
    class Server
    {
        private IPAddress _currentId;
        
        public static Dictionary<ClientHandler, Thread> Handlers { get; set; }
        public static Dictionary<SessionHandler, Thread> Sessions { get; set; }

        private readonly Log _sessionLog;

        private readonly TcpListener _listener;

        /// <summary>
        /// The constructor for a server that hosts a game of ChoHan. It also start a new log for the server. It keeps track of all sessions and client handlers.
        /// </summary>
        public Server()
        {
            //looking for ip
            IPAddress localIp = GetLocalIpAddress();
            Handlers = new Dictionary<ClientHandler, Thread>();
            Sessions = new Dictionary<SessionHandler, Thread>();

            string logName = "SessionLog/" + DateTime.Today + "/" + DateTime.Now + "/ID=" + Handlers.Count;
            _sessionLog = new Log(logName);
            _sessionLog.AddLogEntry("Starting the server.");

            bool ipOk = IPAddress.TryParse(localIp.ToString(), out _currentId);
            if (!ipOk)
            {
                Console.WriteLine("Couldn't parse the ip address. Exiting code.");
                _sessionLog.AddLogEntry("Failed to start. Shuttting down ");
                _sessionLog.PrintLog();
                Environment.Exit(1);
            }

            _listener = new TcpListener(_currentId, 1337);
            _listener.Start();
            _sessionLog.AddLogEntry("Started.");

          
        }

        /// <summary>
        /// This method is all the code the server runs. This loop only adds client handlers when a client connects to the server and makes sure that the connection to the client is stable.
        /// </summary>
        public void Run()
        {
            //making client handlers and adding them to the list
            while (true)
            {
                ClientHandler handler = new ClientHandler(CheckForPlayers(_listener), _sessionLog);
                var thread = new Thread(handler.HandleClientThread);
                thread.Start();
                Handlers.Add(handler, thread);
                CheckSessions();
                SendSessions();
                _sessionLog.AddLogEntry("Started a new thread");
            }
        }
        /// <summary>
        /// A support method for the server.
        /// </summary>
        /// <param name="listner">The TcpListner that needs to be monitored.</param>
        /// <returns>A player connecting to the TcpListner.</returns>
        private Player CheckForPlayers(TcpListener listner)
        { 
            //Looking for players
            Console.WriteLine("Waiting for player");
            var client = listner.AcceptTcpClient();
            Console.WriteLine("Player connected!!");
            dynamic message = SharedUtil.ReadMessage(client);
            var player =  new Player((string)message.data.name, client, 0);
            
            
            _sessionLog.AddLogEntry("Added a player.");
            return player;
        }

        /// <summary>
        /// //A method to get the local ip address of this server. On an exception it will give a reply confirming there is no ip address found.
        /// </summary>
        /// <returns>If possible, the ip address for the server.</returns>
        public static IPAddress GetLocalIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                    return ip;
            throw new Exception("Local IP Address Not Found!");
        }

        /// <summary>
        /// The method to create a session handler.
        /// </summary>
        /// <param name="sessionName">The name of the session that needs a handler.</param>
        /// <returns>The session handler for a the given session.</returns>
        public static SessionHandler FindSession(string sessionName)
        {
            SessionHandler session = null;
            foreach (var s in Sessions)
            {
                if (sessionName.Equals(s.Key.SessionName))
                {
                    session = s.Key;
                }
            }
            return session;
        }

        /// <summary>
        /// The method to send all sessions to a player that want to connect to a session.
        /// </summary>
        public static void SendSessions()
        {
            foreach (var c in Handlers)
            {
                SharedUtil.SendMessage(c.Key.Client.Client, new
                {
                    id = "send/session",
                    data = new
                    {
                        sessions = Sessions.Keys.Select(s => s.ToString()).ToArray()
                    }
                });
            }
        }

        /// <summary>
        /// //The method to send the sessions to a specific TcpClient.
        /// </summary>
        /// <param name="client">The TcpClient that needs the session list.</param>
        public static void SendSessionsToClient(TcpClient client)
        {
            SharedUtil.SendMessage(client, new
            {
                id = "send/session",
                data = new
                {
                    sessions = Sessions.Keys.Select(s => s.ToString()).ToArray()
                }
            });
        }

        /// <summary>
        /// The support method that checks for a session. If there is now session a session will be created. If all sessions are full, a new session will be created.
        /// </summary>
        public static void CheckSessions()
        {
            if (Sessions.Count == 0)
            {
                Console.WriteLine("add session");
                AddSession();
                return;
            }

            bool allSessionsFull = true;
            foreach (var s in Sessions)
            {
                if (s.Key.MaxPlayers != s.Key.Players.Count)
                {
                    allSessionsFull = false;
                }
            }

            if (allSessionsFull)
            {
                AddSession();
            }
        }

        /// <summary>
        /// The support method to check for sessions that can be removed.
        /// </summary>
        public static void CheckRemoveableSessions()
        {
            SessionHandler removeSession = null;
            foreach (var s in Sessions)
            {
                if (s.Key._isInterupted)
                {
                    removeSession = s.Key;
                }
            }

            if (removeSession != null)
            {
                Sessions.Remove(removeSession);
                CheckRemoveableSessions();
            }
            RenameSessions();
        }

        /// <summary>
        /// The support method to easily rename all sessions.
        /// </summary>
        public static void RenameSessions()
        {
            int i = 1;
            foreach (var s in Sessions)
            {
                s.Key.SessionName = $"General {i}";
            }
        }

        /// <summary>
        /// The support method to add a session to the list.
        /// </summary>
        public static void AddSession()
        {
            SessionHandler session = new SessionHandler($"General {Sessions.Count + 1}", 8);
            Thread thread = new Thread(session.SessionHandleThread);

            thread.Start();

            Server.Sessions.Add(session, thread);
            SendSessions();
        }

        /// <summary>
        /// The support method to delete a session.
        /// </summary>
        /// <param name="session">The session to delete.</param>
        public static void DeleteSession(SessionHandler session)
        {
            Server.AddSession();
            Server.Sessions[session].Interrupt();
            Server.Sessions[session].Abort();
        }
    }
}
