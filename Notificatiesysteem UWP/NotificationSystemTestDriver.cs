﻿//The using statement for this namespace.
#region

using System;
using System.Diagnostics;
using Windows.ApplicationModel;

#endregion

//The namespace from which this code was taken.
namespace VVVOnTheWay.NotificationSystem
{
    /// <summary>
    /// The test driver for the notification system.
    /// </summary>
    internal static class NotificationSystemTestDriver
    {
        /// <summary>
        /// The test method. This runs a complete test.
        /// </summary>
        public static async void TestRun()
        {
            var file = await Package.Current.InstalledLocation.GetFileAsync(@"Assets\Netherlands.png");
            var testNotification = new Notification("Test 1", "Test 1");
            var poiNotification = new PoiNotification("Test2", file.Path, "Test2");

            Debug.WriteLine("Started the test");
            Test1(testNotification);
            Test2(testNotification);
            Test3(poiNotification);
            Test4();
            Debug.WriteLine("Finished the test");
        }

        /// <summary>
        /// A test for a pop up notification.
        /// </summary>
        /// <param name="n">The notification to be used.</param>
        public static async void Test1(Notification n)
        {
            Debug.WriteLine("First test");
            await NotificationSystem.SendPopUpNotificationAsync(n);
        }

        /// <summary>
        /// A test for a toast notification.
        /// </summary>
        /// <param name="n">The notification to be used.</param>
        public static void Test2(Notification n)
        {
            Debug.WriteLine("Second test");
            NotificationSystem.SenToastificationAsync(n);
        }

        /// <summary>
        /// A test for a poi toast notification.
        /// </summary>
        /// <param name="n">The notification to be used.</param>
        public static void Test3(PoiNotification n)
        {
            Debug.WriteLine("Third test");
            NotificationSystem.SenToastificationAsync(n);
        }

        /// <summary>
        /// A test for a vibration notification.
        /// </summary>
        /// <param name="n">The notification to be used.</param>
        public static void Test4()
        {
            Debug.WriteLine("Fourth Test");
            NotificationSystem.SendVibrationNotificationAsync();
        }
    }
}